<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
//  /**
//   * @Route("/", name="homepage")
//   *
//   */
//  public function index()
//  {
//    return $this->render('default/index.html.twig');
//  }

  /**
   * @Route("/",name="job_index")
   * @Route("/{vueRouting}", requirements={"vueRouting"="^(?!api|_(profiler|wdt)).*"}, name="job_index_vue")
   */
  public function jobsAction(Request $request)
  {
    return $this->render('jobs/index.html.twig');
  }
}
