<?php
namespace AppBundle\Services;

use AppBundle\Util\ServiceUtil;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AppService
{
  private $loginUser;
  private $request;
  private $entityManager;
  private $container;
  private $translator;

  public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack, TokenStorageInterface $tokenStorage, ContainerInterface $container)
  {
    $this->container = $container;
    $token = $tokenStorage->getToken();
    $this->entityManager = $entityManager;
    $this->request = $requestStack->getCurrentRequest();

    if (!empty($token)) {
      $this->loginUser = $token->getUser();
    }

    $this->translator = $this->container->get(ServiceUtil::TRANSLATOR_SERVICE);
  }

  public function getLoginUser()
  {
    return $this->loginUser;
  }

  public function isAuthenticatedFully()
  {
    return $this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
  }

  public function getRequest()
  {
    return $this->request;
  }

  public function getEntityManager()
  {
    return $this->entityManager;
  }

  public function getContainer()
  {
    return $this->container;
  }

  public function getTranslator()
  {
    return $this->translator;
  }

  public function saveEntity($entity)
  {
    $this->entityManager->persist($entity);
    $this->entityManager->flush();
  }

  public function deleteEntity($entity)
  {
    $this->entityManager->remove($entity);
    $this->entityManager->flush();
  }
}