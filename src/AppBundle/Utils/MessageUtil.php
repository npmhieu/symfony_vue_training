<?php

namespace AppBundle\Utils;

class MessageUtil
{
  const SUCCESS_KEY = 'success';
  const ERROR_KEY = 'error';
  const SUCCESS = 200;
  const FAIL = 400;

  public static function formatMessage($code, $message, $data = null)
  {
    $output = array('code' => $code, 'message' => $message);

    if (!empty($data)) {
      $output['data'] = $data;
    }

    return $output;
  }
}