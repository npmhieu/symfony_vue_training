<?php

namespace AppBundle\Utils;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class SecurityUtil
{

  const UUID_NAME = 'argonbox.com';

  public static function generateUuid($version = 5)
  {
    try {

      switch ($version) {
        case 1:
          // Generate a version 1 (time-based) UUID object
          $uuid1 = Uuid::uuid1();
          return $uuid1->toString(); // i.e. e4eaaaf2-d142-11e1-b3e4-080027620cdd
          break;
        case 3:
          // Generate a version 3 (name-based and hashed with MD5) UUID object
          $uuid3 = Uuid::uuid3(Uuid::NAMESPACE_DNS, self::UUID_NAME);
          return $uuid3->toString(); // i.e. 11a38b9a-b3da-360f-9353-a5a725514269
          break;
        case 4:
          // Generate a version 4 (random) UUID object
          $uuid4 = Uuid::uuid4();
          echo $uuid4->toString(); // i.e. 25769c6c-d34d-4bfe-ba98-e0ee856f3e7a
          break;
        case 5:
          // Generate a version 5 (name-based and hashed with SHA1) UUID object
          $uuid5 = Uuid::uuid5(Uuid::NAMESPACE_DNS, self::UUID_NAME);
          echo $uuid5->toString(); // i.e. c4a760a8-dbcf-5254-a0d9-6a4474bd1b62
          break;
        default:

      }

    } catch (UnsatisfiedDependencyException $e) {

      // Some dependency was not met. Either the method cannot be called on a
      // 32-bit system, or it can, but it relies on Moontoast\Math to be present.
      echo 'Caught exception: ' . $e->getMessage();

    }
  }
}