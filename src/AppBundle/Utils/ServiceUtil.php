<?php

namespace AppBundle\Utils;

class ServiceUtil
{
  const TOKEN_STORAGE = 'security.token_storage';
  const TRANSLATOR_SERVICE = 'translator';
  const PAGINATOR_SERVICE = 'knp_paginator';
}