<?php

namespace AppBundle\Utils;

class StatusUtil
{
  const STATUS_ACTIVE = 1;
  const STATUS_INACTIVE = 0;
  const STATUS_PENDING = 2;

  const STATUS_ACTIVE_TITLE = 'Active';
  const STATUS_INACTIVE_TITLE = 'Inactive';
  const STATUS_PENDING_TITLE = 'Pending';

  const BASIC_STATUS = array(
    self::STATUS_ACTIVE => self::STATUS_ACTIVE_TITLE,
    self::STATUS_INACTIVE => self::STATUS_INACTIVE_TITLE,
    self::STATUS_PENDING => self::STATUS_PENDING_TITLE,
  );

  const BASIC_STATUS_FLIP = array(
    self::STATUS_ACTIVE_TITLE => self::STATUS_ACTIVE,
    self::STATUS_INACTIVE_TITLE => self::STATUS_INACTIVE,
    self::STATUS_PENDING_TITLE => self::STATUS_PENDING,
  );
}