<?php
// api/src/Entity/Review.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

///**
// * A review of a book.
// *
// * @ORM\Entity
// * @ApiResource(
// *   attributes={
// *      "normalization_context"={"groups"={"read"}},
// *      "denormalization_context"={"groups"={"write"}}
// *   }
// * )
// *
// */
class Review
{
  /**
   * @var int The id of this review.
   *
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   * @Groups({"read"})
   */
  private $id;

  /**
   * @var int The rating of this review (between 0 and 5).
   *
   * @ORM\Column(type="smallint")
   * @Groups({"read","write"})
   *
   */
  public $rating;

  /**
   * @var string the body of the review.
   *
   * @ORM\Column(type="text")
   * @Groups({"read","write"})
   *
   */
  public $body;

  /**
   * @var string The author of the review.
   *
   * @ORM\Column
   * @Groups({"read","write"})
   *
   */
  public $author;

  /**
   * @var \DateTimeInterface The date of publication of this review.
   *
   * @ORM\Column(type="datetime")
   * @Groups({"read","write"})
   *
   */
  public $publicationDate;

  /**
   * @var Book The book this review is about.
   *
   * @ORM\ManyToOne(targetEntity="Book", inversedBy="reviews")
   * @Groups({"write"})
   *
   */
  public $book;

  public function getId(): ?int
  {
    return $this->id;
  }

  /**
   * @return int
   */
  public function getRating()
  {
    return $this->rating;
  }

  /**
   * @param int $rating
   */
  public function setRating($rating)
  {
    $this->rating = $rating;
  }

  /**
   * @return string
   */
  public function getBody()
  {
    return $this->body;
  }

  /**
   * @param string $body
   */
  public function setBody($body)
  {
    $this->body = $body;
  }

  /**
   * @return string
   */
  public function getAuthor()
  {
    return $this->author;
  }

  /**
   * @param string $author
   */
  public function setAuthor($author)
  {
    $this->author = $author;
  }

  /**
   * @return \DateTimeInterface
   */
  public function getPublicationDate()
  {
    return $this->publicationDate;
  }

  /**
   * @param \DateTimeInterface $publicationDate
   */
  public function setPublicationDate($publicationDate)
  {
    $this->publicationDate = $publicationDate;
  }

  /**
   * @return Book
   */
  public function getBook()
  {
    return $this->book;
  }

  /**
   * @param Book $book
   */
  public function setBook($book)
  {
    $this->book = $book;
  }

  /**
   * @Groups({"read"})
   */
  public function getBookTitle(){
    return empty($this->book) ? '' : $this->book->getTitle();
  }
}