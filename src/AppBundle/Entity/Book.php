<?php
// api/src/Entity/Book.php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

///**
// * A book.
// *
// * @ORM\Entity
// * @ApiResource(
// *   attributes={
// *      "normalization_context"={"groups"={"read"}},
// *      "denormalization_context"={"groups"={"write"}}
// *   }
// * )
// */
class Book
{
  /**
   * @var int The id of this book.
   *
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   * @Groups({"read"})
   */
  private $id;

  /**
   * @var string|null The ISBN of this book (or null if doesn't have one).
   *
   * @ORM\Column(nullable=true)
   * @Groups({"read","write"})
   */
  public $isbn;

  /**
   * @var string The title of this book.
   *
   * @ORM\Column
   * @Groups({"read","write"})
   *
   */
  public $title;

  /**
   * @var string The description of this book.
   *
   * @ORM\Column(type="text")
   * @Groups({"read","write"})
   */
  public $description;

  /**
   * @var string The author of this book.
   *
   * @ORM\Column
   * @Groups({"read","write"})
   */
  public $author;

  /**
   * @var \DateTimeInterface The publication date of this book.
   *
   * @ORM\Column(type="datetime")
   * @Groups({"read","write"})
   */
  public $publicationDate;

  /**
   * @var Review[] Available reviews for this book.
   *
   * @ORM\OneToMany(targetEntity="Review", mappedBy="book", cascade={"persist"})
   */
  public $reviews;

  public function __construct()
  {
    $this->reviews = new ArrayCollection();
  }

  public function getId(): ?int
  {
    return $this->id;
  }

  /**
   * @return null|string
   */
  public function getIsbn()
  {
    return $this->isbn;
  }

  /**
   * @param null|string $isbn
   */
  public function setIsbn($isbn)
  {
    $this->isbn = $isbn;
  }

  /**
   * @return string
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * @param string $title
   */
  public function setTitle($title)
  {
    $this->title = $title;
  }

  /**
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * @return string
   */
  public function getAuthor()
  {
    return $this->author;
  }

  /**
   * @param string $author
   */
  public function setAuthor($author)
  {
    $this->author = $author;
  }

  /**
   * @return \DateTimeInterface
   */
  public function getPublicationDate()
  {
    return $this->publicationDate;
  }

  /**
   * @param \DateTimeInterface $publicationDate
   */
  public function setPublicationDate($publicationDate)
  {
    $this->publicationDate = $publicationDate;
  }

  /**
   * @return Review[]
   */
  public function getReviews()
  {
    return $this->reviews;
  }

  /**
   * @param Review[] $reviews
   */
  public function setReviews($reviews)
  {
    $this->reviews = $reviews;
  }


}