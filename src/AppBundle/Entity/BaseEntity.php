<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Carbon\Carbon;

/**
 * @ORM\MappedSuperclass()
 * @ORM\HasLifecycleCallbacks
 */
class BaseEntity
{

  /**
   * @var \DateTime
   * @ORM\Column(name="created", type="datetime")
   */
  private $created;

  /**
   * @var \DateTime
   * @ORM\Column(name="updated", type="datetime", nullable=true)
   */
  private $updated;

  /**
   * @ORM\PrePersist
   * @return void
   */
  public function onPrePersist(): void
  {
    $this->created = Carbon::now();
  }

  /**
   * @ORM\PreUpdate
   * @return void
   */
  public function onPreUpdate(): void
  {
    $this->updated = Carbon::now();
  }


  public function getCreated()
  {
    return $this->created;
  }

  public function setCreated($created)
  {
    $this->created = $created;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getUpdated()
  {
    return $this->updated;
  }

  /**
   * @param mixed $updated
   */
  public function setUpdated($updated)
  {
    $this->updated = $updated;
  }


}

