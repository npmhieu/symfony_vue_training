import './css/common.css';
import './css/style.css';

import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false;

import router from './routes';

new Vue({
  router,
  template: '<App/>',
  components: { App }
}).$mount('#app');

// new Vue({
//   render: h => h(App),
// }).$mount('#app')

