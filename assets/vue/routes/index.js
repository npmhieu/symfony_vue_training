import Vue from 'vue';
import VueRouter from 'vue-router';

import HomeIndex from '../components/HelloWorld.vue';
import JobListIndex from '../components/JobList.vue';
import JobDetailIndex from '../components/JobDetail.vue';
import PageNotFound from '../components/NotFound.vue';


Vue.use(VueRouter);

const routes = [
    {path: '/', component: HomeIndex, name: 'home-index'},
    {path: '/jobs', component: JobListIndex, name: 'jobs-index'},
    {path: '/jobs/:job_id', component: JobDetailIndex, name: 'job-detail-index'},
    {path: '/*', component: PageNotFound, name: 'page-not-found'}
];

export default new VueRouter({
    mode: 'history',
    routes
});