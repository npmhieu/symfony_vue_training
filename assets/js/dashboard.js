var dashboardJS = {
  initComponents: function () {
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  },
  init: function () {
    this.initComponents();
  }
};

$(function () {
  dashboardJS.init();
});